@api @degov_media_usage @entities
Feature: deGov - Media usage

  Background:
    Given I proof that the following Drupal modules are installed:
      | degov_media_usage |
    Given I am logged in as a user with the "administrator" role
    Given I have dismissed the cookie banner if necessary

    @degov_media_usage_smoketest
  Scenario: As a administrator I want to verify the expected texts
    And I am on "/admin/structure/views/nojs/add-handler/media/media_page_list/field"
    Then I should see "Media usage count"
    And I am on "/admin/content/media"
    Then I should see HTML content matching "Used in" via translated text
    And I should see text matching "2 Stellen" in "css" selector ".media-usage"
    When I am on "/admin/content/media/refs/12"
    Then I should see text matching 'Page with download paragraph > Inhaltsbereich' in "css" selector "td"
    And I should see text matching 'Page with references to all types of media' in "css" selector "td a"
    And I should see text matching 'Medienreferenzen von "Just a word document" durchsuchen' in "css" selector "h1"
    And I should see text matching 'Dieses Medium wird 3-mal verwendet.' in "css" selector "h4"

    @degov_media_usage_entity_embed @javascript
    Scenario: I want to verify that media entities are tracked/untracked in text fields of embed entity via usecase 1
      Given I am installing the "degov_media_usage_entity_embed" module
      And I am on "/node/add/normal_page"
      And I fill in "Titel" with "degov_media_usage_entity_embed"
      Then I should see 1 ".cke" elements via jQuery after a while
      When I click "Medien-Browser"
      Then I should see HTML content matching "entity_browser_iframe_ckeditor_media_browser" after a while
      And I should not see HTML content matching "Demo image with a fixed title"
      When I switch to the "entity_browser_iframe_ckeditor_media_browser" frame
      Then I should see HTML content matching "Demo image with a fixed title"
      # First media element
      When I click by selector "div.views-view-grid:nth-child(1) > div:nth-child(1) > div:nth-child(1)" via JavaScript
      And I scroll to bottom
      And I press button with label "Place" via translated text
      And I scroll to bottom
      # Paste button. We have 2 buttons with label Paste therefore use a selector.
      And I click by selector "button.button:nth-child(2)" via JavaScript
      And I scroll to bottom
      And I press button with label "Save" via translated text
      Then I should have 25 items in table degov_media_usage for submodule "degov_media_usage_entity_embed"
      When I open node edit form by node title "degov_media_usage_entity_embed"
      And I click by selector ".cke_button__source" via JavaScript
      And I set the value of element "#edit-field-teaser-text-wrapper .form-textarea-wrapper .cke_source" to "REMOVED" via JavaScript
      And I scroll to bottom
      And I press button with label "Save" via translated text
      Then I should have 24 items in table degov_media_usage for submodule "degov_media_usage_entity_embed"
      When I open node delete form by node title "degov_media_usage_entity_embed"
      And I press button with label "Delete" via translated text

    @degov_media_usage_entity_embed @javascript
    Scenario: I want to verify that media entities are tracked/untracked in text fields of embed entity via usecase 2
      Given I am installing the "degov_media_usage_entity_embed" module
      And I am on "/node/add/normal_page"
      And I fill in "Titel" with "degov_media_usage_entity_embed"
      Then I should see 1 ".cke" elements via jQuery after a while
      When I click "Medien-Browser"
      Then I should see HTML content matching "entity_browser_iframe_ckeditor_media_browser" after a while
      And I should not see HTML content matching "Demo image with a fixed title"
      When I switch to the "entity_browser_iframe_ckeditor_media_browser" frame
      Then I should see HTML content matching "Demo image with a fixed title"
        # First media element
      When I click by selector "div.views-view-grid:nth-child(1) > div:nth-child(1) > div:nth-child(1)" via JavaScript
      And I scroll to bottom
      And I press button with label "Place" via translated text
      And I scroll to bottom
        # Paste button. We have 2 buttons with label Paste therefore use a selector.
      And I click by selector "button.button:nth-child(2)" via JavaScript
      And I scroll to bottom
      And I press button with label "Save" via translated text
      Then I should have 25 items in table degov_media_usage for submodule "degov_media_usage_entity_embed"
      When I open node delete form by node title "degov_media_usage_entity_embed"
      And I press button with label "Delete" via translated text
      Then I should have 24 items in table degov_media_usage for submodule "degov_media_usage_entity_embed"
