@api @drupal @access
Feature: deGov - Workbench related tests

  Scenario: As a editor I can access the workbench overview
    Given I am logged in as a user with the "editor" role
    And I am at "admin/workbench"
    Then I should see "Mein Arbeitsbereich"

  Scenario: As a manager I can access the workbench overview
    Given I am logged in as a user with the "manager" role
    And I am at "admin/workbench"
    Then I should see "Mein Arbeitsbereich"
