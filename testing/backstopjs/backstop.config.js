module.exports = {
  id: 'backstop_default',
  viewports: [
    {
      label: 'phone',
      width: 320,
      height: 480
    },
    {
      label: 'tablet-portrait',
      width: 768,
      height: 1024,
    },
    {
      label: 'tablet-landscape',
      width: 1024,
      height: 768
    },
    {
      label: 'desktop',
      width: 1280,
      height: 768
    }
  ],
  onBeforeScript: 'puppet/onBefore.js',
  onReadyScript: 'puppet/onReady.js',
  disableJavascript: false,
  referenceUrl: '',
  readyEvent: '',
  readySelector: '',
  hideSelectors: [],
  postInteractionWait: 0,
  misMatchThreshold: 2,
  requireSameDimensions: false,
  selectorExpansion: true,
  expect: 0,
  scenarios: [
    {
      label: 'Frontpage',
      cookiePath: 'backstop_data/cookies/acceptedAllSocialMedia.json',
      _comment: 'Instagram images are hidden',
      url: 'http://host.docker.internal',
      referenceUrl: '',
      readyEvent: '',
      readySelector: '#degov-slider .carousel-item:nth-child(2).active',
      removeSelectors: [
        'header',
        '.alert-dismissible',
        '.site-footer',
        '.footer-bottom-wrapper',
        '#sliding-popup',
      ]
    },
    {
      label: 'video embed no cookies',
      _comment: 'Includes header and footers and cookie banner',
      url: 'http://host.docker.internal/degov-demo-content/page-video-header',
      scrollToSelectors: [
        '.footer-bottom-wrapper'
      ],
    },
    {
      label: 'video embed, cookies accepted, social media allowed: none',
      _comment: 'Includes header and footers.',
      cookiePath: 'backstop_data/cookies/acceptedNoSocialMedia.json',
      url: 'http://host.docker.internal/degov-demo-content/page-video-header',
      scrollToSelectors: [
        '.footer-bottom-wrapper'
      ],
    },
    {
      label: 'video embed, all social media',
      cookiePath: 'backstop_data/cookies/acceptedAllSocialMedia.json',
      url: 'http://host.docker.internal/degov-demo-content/page-video-header',
      postInteractionWait: 3000,
      scrollToSelectors: [
        '.js-social-media-code',
      ],
      readySelector: '#video-embed-plyr-0', // BG image loaded.
    },
    {
      label: 'Social Media Settings popup',
      _comment: 'Show Popup with twitter selected',
      url: 'http://host.docker.internal/degov-demo-content/page-block-reference',
      cookiePath: 'backstop_data/cookies/acceptedNoSocialMedia.json',
      onReadyScript: './puppet/verifyOverlaySocialMediaSettings.js',
      selectors: [
        '.modal-dialog'
      ]
    },
    {
      label: 'page-map-paragraph',
      _comment: 'Google and OSM maps (and dismiss google dev banner 2x)',
      url: 'http://host.docker.internal/degov-demo-content/page-map-paragraph',
      onReadyScript: './puppet/verifyMapsPopups.js',
      delay: 1000,
      postInteractionWait: 500,
      removeSelectors: [
        'header',
        '.alert-dismissible',
        '.site-footer',
        '.footer-bottom-wrapper',
        '#sliding-popup',
      ],
    },
    {
      label: 'Admin Cookie',
      _comment: 'View people/roles which is only visible for Admin users',
      url: 'http://host.docker.internal/admin/people/roles',
      cookiePath: 'tmp/cookiesAdmin.json',
      delay: 4000,
      removeSelectors: [
        '#toolbar-administration',
        '.sf-toolbarreset',
        '.sf-minitoolbar',
        '[data-drupal-messages]'
      ],
      skipValidation: true
    },
    {
      label: 'page-media-reference-some-embed WITH Social Media enabled',
      _comment: 'Migrated from page-references-all-types-media',
      url: 'http://host.docker.internal/degov-demo-content/page-media-reference-some-embed',
      cookiePath: 'backstop_data/cookies/acceptedAllSocialMedia.json',
      scrollToSelectors: [
        '.paragraph__content',
      ],
      postInteractionWait: 3000,
      readySelector: '.twitter-tweet-rendered',
      removeSelectors: [
        'header',
        '.alert-dismissible',
        '.site-footer',
        '.footer-bottom-wrapper',
        '#sliding-popup',
      ],
    },
    {
      label: 'page-media-reference-some-embed WITHOUT Social Media enabled',
      _comment: 'Migrated from page-references-all-types-media',
      url: 'http://host.docker.internal/degov-demo-content/page-media-reference-some-embed',
      cookiePath: 'backstop_data/cookies/acceptedNoSocialMedia.json',
      removeSelectors: [
        'header',
        '.alert-dismissible',
        '.site-footer',
        '.footer-bottom-wrapper',
        '#sliding-popup',
      ],
    },
    {
      label: 'page-media-reference-tweet WITH Social Media enabled',
      _comment: 'Migrated from page-references-all-types-media',
      url: 'http://host.docker.internal/degov-demo-content/page-media-reference-tweet',
      cookiePath: 'backstop_data/cookies/acceptedAllSocialMedia.json',
      scrollToSelectors: [
        '.js-social-media-wrapper',
      ],
      postInteractionWait: 3000,
      readySelector: '.twitter-tweet-rendered',
      removeSelectors: [
        'header',
        '.alert-dismissible',
        '.site-footer',
        '.footer-bottom-wrapper',
        '#sliding-popup',
      ],

    },
    {
      label: 'page-media-reference-tweet WITHOUT Social Media enabled',
      _comment: 'Migrated from page-references-all-types-media',
      url: 'http://host.docker.internal/degov-demo-content/page-media-reference-tweet',
      cookiePath: 'backstop_data/cookies/acceptedNoSocialMedia.json',
      removeSelectors: [
        'header',
        '.alert-dismissible',
        '.site-footer',
        '.footer-bottom-wrapper',
        '#sliding-popup',
      ],
    },
  ],
  paths: {
    bitmaps_reference: 'backstop_data/bitmaps_reference',
    bitmaps_test: 'backstop_data/bitmaps_test',
    engine_scripts: 'backstop_data/engine_scripts',
    html_report: 'backstop_data/html_report',
    ci_report: 'backstop_data/ci_report'
  },
  report: [
    'browser'
  ],
  engine: 'puppeteer',
  engineOptions: {
    args: [
      '--no-sandbox'
    ],
    chromeFlags: ['--disable-gpu', '--force-device-scale-factor=1']
  },
  asyncCaptureLimit: 3,
  asyncCompareLimit: 50,
  debug: false,
  debugWindow: false
}
