/**
 * @file
 */

module.exports = async(page, scenario, vp) => {
  await require('./onReadyInfo')(page, scenario, vp);
  await require('./clickAndHoverHelper')(page, scenario, vp);
  await require('./onReadyWaitForImages')(page, scenario, vp);

  await page.waitForSelector("div[data-social-media-source='instagram'] a[data-target='#social-media-settings']");
  await page.click("div[data-social-media-source='instagram'] a[data-target='#social-media-settings']");
  await page.waitForSelector(
    "#checkbox-twitter", {
      timeout: 6000
    }
  );
  await page.waitForTimeout(500);
  await page.evaluate(async() => {
    jQuery("#checkbox-twitter + label").click();
    setTimeout(() => {
      jQuery('#checkbox-twitter + label').focus()
    }, 500);
  });
  await new Promise(resolve => setTimeout(resolve, 300));
};
