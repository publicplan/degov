/* jshint esversion: 8, node: true */

module.exports = async (page, scenario, vp) => {
  'use strict';
  var postInteractionWait = scenario.postInteractionWait;

  await require('./onReadyInfo')(page, scenario, vp);
  // await require('./clickAndHoverHelper')(page, scenario, vp);
  await require('./onReadyWaitForImages')(page, scenario, vp);

  // Wait for init
  await page.waitForSelector('.js');

  await page.setViewport(vp);

  // What a silly way to test. It seems to be a feature
  // @see docroot/core/modules/contextual/tests/src/FunctionalJavascript/ContextualLinkClickTrait.php
  // executes:
  // jQuery('{$selector} .contextual .trigger').toggleClass('visually-hidden');
  // before test.
  await page.waitForTimeout(1000);

  await page.evaluate(() => {
    const contextuaLink = window.jQuery('.paragraph.contextual-region').last();
    contextuaLink.unbind('mouseleave'); // Contextual link closes in Pupeteer when hovering the child element.
    contextuaLink.find('.contextual .trigger').removeClass('visually-hidden').trigger('click');
    contextuaLink.find('.contextual .paragraphs-editedit-form a').attr('data-selector-should-hover', '1');
  });
  // For unknown reason hover doe not work in small viewport.
  await page.hover('.contextual .paragraphs-editedit-form a[data-selector-should-hover]');

  if (postInteractionWait) {
    await page.waitForTimeout(postInteractionWait);
  }
};
