/**
 * @file
 */

module.exports = async(page, scenario, vp) => {
  await require('./onReadyInfo')(page, scenario, vp);
  await require('./clickAndHoverHelper')(page, scenario, vp);
  await require('./onReadyWaitForImages')(page, scenario, vp);

  const prefix = '.media-gallery';
  // Wait for init.
  await page.waitForSelector(`${prefix} .slick-initialized`);

  // Open lightbox overlay.
  await Promise.all([
    page.waitForNavigation({'waitUntil': 'networkidle0'}),
    page.click(`${prefix} .slick-slider.slick-initialized + .slick-controls__gallery button.slick__lightroom`),
  ]);

  // Open share menu.
  await page.click(`${prefix} .pswp__button--share`);
  await page.hover(`${prefix} .pswp__share--twitter`);
  // Wait for css animation.
  await new Promise(resolve => setTimeout(resolve, 300));
};
