/* jshint esversion: 8, node: true */


module.exports = async (page, scenario, vp, isReference, pupeteer) => {
  var postInteractionWait = scenario.postInteractionWait;

  await require('./onReadyInfo')(page, scenario, vp);
  await require('./clickAndHoverHelper')(page, scenario, vp);
  await require('./onReadyWaitForImages')(page, scenario, vp);

  // Wait for init
  await page.waitForSelector('.slick-slider.slick-initialized');
  await page.setViewport(vp);


  // We can not do that as of https://github.com/garris/BackstopJS/issues/948.
  // So we need to hardcode each slide.
  // let currentSlide = 0;
  // const numberOfSlides= await page.evaluate(() => {
  //   return jQuery('.slick-slider')[0].slick.$slides.length;
  // });
  // console.log('numberOfSlides ' + numberOfSlides);
  // page.exposeFunction('afterChange', function(event, slick, thisSlide, nextSlide){
  //   console.log(thisSlide, 'afterChange');
  // });
  // await page.evaluate((currentSlide) => {
  //   jQuery('.slick-slider').slick('slickSetOption', 'afterChange', window.afterChange);
  // });
  // // @see https://github.com/garris/BackstopJS/issues/820#issuecomment-404963122
  // const screens = [];
  // while (currentSlide < numberOfSlides) {
  //   await page.evaluate(async (currentSlide) => {
  //     const TEST = await jQuery('.slick-slider').slick('slickGoTo', currentSlide);
  //     console.log('TAKEING PICTURE NO ' + currentSlide, TEST);
  //   }, currentSlide);
  //
  //   const screen = await page.screenshot({path: `slide-${currentSlide}.png`, fullPage: true});
  //   screens.push(screen);
  //   currentSlide = currentSlide +1;
  // }
  // // const img = await merge(screens, {direction: true, offset: 20, margin: 10, color: 0xFF0000FF});
  // // img.write('out.png');
  // await page.browser.close();

  if (!Number.isInteger(scenario.scrollToSlide)) {
    throw 'A scenario involving verifySlideshowContent.js must have a scenario.scrollToSlide integer value set.'
  }
  await page.evaluate(async (currentSlide) => {
    await jQuery('.slick-slider').slick('slickGoTo', currentSlide, true);
  }, scenario.scrollToSlide);
  await page.waitForTimeout(1000);

  if (postInteractionWait) {
    await page.waitForTimeout(postInteractionWait);
  }
};
