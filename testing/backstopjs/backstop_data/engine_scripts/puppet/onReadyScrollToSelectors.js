/**
 * @file
 */

module.exports = async(page, scenario, vp) => {

  // Scroll every element into view.
  // E.g. tweets are only fully loaded when scrolling into view.
  if (scenario.scrollToSelectors && Array.isArray(scenario.scrollToSelectors)) {
    for (const sel of scenario.scrollToSelectors) {
      await page.waitForSelector(sel);
      await page.evaluate(scrollToSelector => {
        document.querySelector(scrollToSelector).scrollIntoView({ behavior: 'smooth' });
        console.log('scrolled')
      }, sel);
      console.log('scrolled done')
      await page.waitForTimeout(5000);
      console.log('scrolled waited')
    }
  }
}
