<?php

/**
 * @file
 * Drupal hooks implementations for the degov_media_video_upload module.
 */

declare(strict_types=1);

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Template\Attribute;
use Drupal\image\ImageStyleInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Implements hook_theme().
 */
function degov_media_video_upload_theme($existing, $type, $theme, $path) {
  return [
    'media__video_upload' => [
      'base hook' => 'media',
      'template' => 'media--video-upload',
    ],
    'media__video_upload__embedded' => [
      'base hook' => 'media',
      'template' => 'media--video-upload--embedded',
    ],
    'media__video_upload__full' => [
      'base hook' => 'media',
      'template' => 'media--video-upload--full',
    ],
    'media__video_upload__preview' => [
      'base hook' => 'media',
      'template' => 'media--video-upload--preview',
    ],
    'media__video_upload__preview_wide' => [
      'base hook' => 'media',
      'template' => 'media--video-upload--preview-wide',
    ],
    'media__video_upload__reference' => [
      'base hook' => 'media',
      'template' => 'media--video-upload--reference',
    ],
    'media__video_upload__search' => [
      'base hook' => 'media',
      'template' => 'media--video-upload--search',
    ],
    'media__video_upload__usage' => [
      'base hook' => 'media',
      'template' => 'media--video-upload--usage',
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function degov_media_video_upload_preprocess_media__video_upload(&$variables) {
  $posterUri = NULL;
  /* @var \Drupal\media\Entity\Media $media */
  $media = $variables['elements']['#media'];
  $variables['video_url'] = $media->toUrl();
  $previewMedia = $media->get('field_video_upload_preview')->first();
  if ($previewMedia) {
    $previewImage = $previewMedia->get('entity')->getTarget()->getValue()->get('image')->first()->entity;
    if ($previewImage) {
      $posterUri = ImageStyle::load('slider_main_16_9_1280')->buildUrl($previewImage->getFileUri());
      $variables['#cache']['tags'] = $previewImage->getCacheTags();
    }
  }
  $variables['video_attributes'] = new Attribute([
    'id'           => 'video-upload-' . $media->id(),
    'preload'      => 'metadata',
    'controls'     => 'controls',
    'controlslist' => 'nodownload',
    'title' => $media->get('field_title')->first()->value,
    'poster' => $posterUri ?? '',
  ]);
  $variables['#attached']['library'][] = 'degov_media_video_upload/video_upload';

  // Check if the download of the video is allowed by media settings.
  $download_is_allowed = FALSE;
  if ($media->hasField('field_allow_download') && !$media->get('field_allow_download')->isEmpty()) {
    $download_is_allowed = $media->get('field_allow_download')->value ? TRUE : FALSE;
  }

  if (!$media->get('field_video_upload_mp4')->isEmpty()) {
    /* @var \Drupal\file\Entity\File $file */
    $file = $media->field_video_upload_mp4->entity;
    if (!empty($file)) {
      $variables['video_mp4'] = file_create_url($file->getFileUri());
      // Add the download link only to full view mode.
      if (in_array($variables['elements']['#view_mode'], ['full', 'default'])) {
        // If allowed let's prepare the links.
        if ($download_is_allowed) {
          $variables['content']['download_mp4'] = degov_common_file_download_render_array($file);
        }
      }
    }
  }

  if (!$media->get('field_video_upload_webm')->isEmpty()) {
    /* @var \Drupal\file\Entity\File $file */
    $file = $media->field_video_upload_webm->entity;
    if (!empty($file)) {
      $variables['video_webm'] = file_create_url($file->getFileUri());
      // Add the download link only to full view mode.
      if (in_array($variables['elements']['#view_mode'], ['full', 'default'])) {
        // If allowed let's prepare the links.
        if ($download_is_allowed) {
          $variables['content']['download_webm'] = degov_common_file_download_render_array($file);
        }
      }
    }
  }

  if (!$media->get('field_video_upload_ogg')->isEmpty()) {
    /* @var \Drupal\file\Entity\File $file */
    $file = $media->field_video_upload_ogg->entity;
    if (!empty($file)) {
      $variables['video_ogg'] = file_create_url($file->getFileUri());
      // Add the download link only to full view mode.
      if (in_array($variables['elements']['#view_mode'], ['full', 'default'])) {
        // If allowed let's prepare the links.
        if ($download_is_allowed) {
          $variables['content']['download_ogg'] = degov_common_file_download_render_array($file);
        }
      }
    }
  }

  /* @var \Drupal\media\Entity\Media $poster */
  $image = $media->field_video_upload_preview->entity;
  if (!empty($image)) {
    $file = $image->image->entity;
    if (!empty($file)) {
      $imageStyle16By9 = \Drupal::entityTypeManager()->getStorage('image_style')->load('crop_16_to_9');
      if ($imageStyle16By9 instanceof ImageStyleInterface) {
        $variables['video_attributes']['poster'] = $imageStyle16By9->buildUrl($file->getFileUri());
      }
      else {
        $variables['video_attributes']['poster'] = file_create_url($file->getFileUri());
      }
    }
  }
  degov_media_plyr_attach_to_render_array($variables);
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function degov_media_video_upload_media_presave(EntityInterface $entity) {
  $bundle = $entity->bundle();
  if ($bundle === 'video_upload') {
    // Check if the field for duration is enabled for the media bundle and that it is empty.
    // There is no need to check the duration again and again if it is already set.
    if ($entity->hasField('field_media_duration') && $entity->get('field_media_duration')->isEmpty()) {
      /** @var \Drupal\degov_common\VideoUtils $videoUtils */
      $videoUtils = \Drupal::service('degov_common.video_utils');
      $duration = $videoUtils->getVideoDuration($entity);
      $entity->set('field_media_duration', [0 => ['value' => $duration]]);
    }
  }
}
