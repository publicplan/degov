<?php

declare(strict_types=1);

namespace Drupal\degov_hyphenopoly\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
final class HyphenopolySettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  public const SETTINGS = 'degov_hyphenopoly.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return self::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config(self::SETTINGS);
    /** @var string[]|null $cssSelectors */
    $cssSelectors = $config->get('hyphenopoly_selectors') ?? [];

    $form['into'] = [
      '#markup' => '<p>' . $this->t('JavaScript-polyfill for hyphenation in HTML based on') . ' <a href="https://github.com/mnater/Hyphenopol">Hyphenopoly</a>.</p>',
    ];

    $form['hyphenopoly_selectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CSS selectors'),
      '#required' => TRUE,
      '#description' => $this->t('Specify one selector per line. For example <code>.normal-page__teaser-title</code> or <code>#my-field</code>. Only class and ID selectors are valid due performance constraints.'),
      '#default_value' => \is_array($cssSelectors) ? $this->selectorsToString($cssSelectors) : NULL,
    ];

    $form['leftmin'] = [
      '#title' => 'Left min',
      '#description' => $this->t('Minimal number of characters before the first hyphenation point.'),
      '#required' => TRUE,
      '#maxlength' => 3,
      '#type' => 'textfield',
      '#default_value' => $config->get('leftmin'),
      '#attributes' => [
        'data-type' => 'number',
        'pattern' => '\d*',
      ],
    ];

    $form['rightmin'] = [
      '#title' => 'Right min',
      '#description' => $this->t('Minimal number of characters after the last hyphenation.'),
      '#required' => TRUE,
      '#maxlength' => 3,
      '#type' => 'textfield',
      '#default_value' => $config->get('rightmin'),
      '#attributes' => [
        'data-type' => 'number',
        'pattern' => '\d*',
      ],

    ];

    $form['orphan_control'] = [
      '#title' => \t('orphanControl'),
      '#description' => $this->t('Prevent <a href="https://en.wikipedia.org/wiki/Widows_and_orphans">orphans</a>. Note that the rightmin option migt work better on narrow sizes.'),
      '#type' => 'select',
      '#default_value' => $config->get('orphan_control'),
      '#options' => [
        1 => $this->t('Allow orphans'),
        2 => $this->t('Do not hyphenate the last word of an element'),
        3 => $this->t('Do not hyphenate the last word of an element and replace the space before with a no-breaking space'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    foreach ($this->selectorsToArray($form_state->getValue('hyphenopoly_selectors')) as $ln => $line) {
      $class_prefix = $line[0];
      if (!($class_prefix === '.' || $class_prefix === '#')) {
        $form_state->setErrorByName('hyphenopoly_selectors', (string) $this->t('CSS selector name must start with a dot or a hash at line @line.', ['@line' => $ln + 1]));
      }
      // @see https://regex101.com/r/mA6cA0/13
      if (!\preg_match('/^(\.?\#?[_a-zA-Z]+[_a-zA-Z0-9-]*)$/', $line)) {
        $form_state->setErrorByName('hyphenopoly_selectors', (string) $this->t('Line number @line is not valid CSS selector', ['@line' => $ln + 1]));
      }
    }
    $leftMin = (int) $form_state->getValue('leftmin');
    if ($leftMin < 0 || $leftMin > 256) {
      $form_state->setErrorByName('leftmin', $this->t('Left min must be a positive integer [0-256]'));
    }

    $rightMin = (int) $form_state->getValue('rightmin');
    if ($rightMin < 0 || $rightMin > 256) {
      $form_state->setErrorByName('rightmin', $this->t('Right min must be a positive integer [0-256]'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config(self::SETTINGS);
    $selectors = $this->selectorsToArray($form_state->getValue('hyphenopoly_selectors'));
    $config->set('hyphenopoly_selectors', $selectors)->save();
    $config->set('leftmin', $form_state->getValue('leftmin'));
    $config->set('rightmin', $form_state->getValue('rightmin'));
    $config->set('orphan_control', $form_state->getValue('orphan_control'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Converts a multiline text field to array.
   *
   * @param string $str
   *   Multiline input string.
   *
   * @return string[]
   */
  protected function selectorsToArray(string $str): array {
    return \explode('\n', \str_replace([
      "\r\n",
      "\n\r",
      "\r",
    ], '\n', \trim($str)));
  }

  /**
   * Convert Array data to multiline string.
   *
   * @param string[] $data
   *
   * @return string
   *   Multiline input string.
   */
  protected function selectorsToString(array $data): string {
    return \implode("\n", $data);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::SETTINGS,
    ];
  }

}
