<?php

declare(strict_types=1);

namespace Drupal\degov_common\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class UserProfileLink.
 *
 * @Block(
 *   id = "degov_user_profile_link",
 *   admin_label = @Translation("Login / Profile link"),
 *   category = @Translation("Blocks")
 * )
 */
final class UserProfileLink extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $block = [
      '#theme' => 'degov_user_profile_link',
      '#cache' => [
        'contexts' => [
          'user',
        ],
      ],
    ];
    return $block;
  }

}
