<?php

/**
 * @file
 * Drupal hooks implementations for the degov_paragraph_video_subtitle module.
 */

declare(strict_types=1);

/**
 * Implements hook_theme().
 */
function degov_paragraph_video_subtitle_theme($existing, $type, $theme, $path) {
  return [
    'paragraph__video_subtitle' => [
      'base hook' => 'paragraph',
      'template' => 'paragraph--video-subtitle',
    ],
    'paragraph__video_subtitle__preview' => [
      'base hook' => 'paragraph',
      'template' => 'paragraph--video-subtitle--preview',
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function degov_paragraph_video_subtitle_preprocess_paragraph__video_subtitle(&$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];

  $attributes = [];

  $label = $paragraph->field_subtitle_label->value;
  if (!empty($label)) {
    $attributes['label'] = $label;
  }

  $kind = $paragraph->field_subtitle_kind->value;
  if (!empty($kind)) {
    $attributes['kind'] = $kind;
  }

  $lang = $paragraph->field_subtitle_lang->value;
  if (!empty($lang)) {
    $attributes['srclang'] = $lang;
  }

  /** @var \Drupal\file\Entity\File $file */
  $file = $paragraph->field_subtitle_file->entity;
  if (!empty($file)) {
    $attributes['src'] = file_create_url($file->getFileUri());
  }

  $default = $paragraph->field_subtitle_default->value;
  if (!empty($default)) {
    $attributes['default'] = 'default';
  }

  $variables['attributes'] = $attributes;
}

// phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName

/**
 * Gets the allowed values for the field_subtitle_lang field.
 *
 * @return array
 *   An array of allowed values.
 */
function getLanguageList() {
  // Used by modules/degov_paragraph_video_subtitle/config/install/field.storage.paragraph.field_subtitle_lang.yml.
  $options = [];
  $languages = \Drupal::languageManager()->getLanguages();

  foreach ($languages as $language) {
    $options[$language->getId()] = $language->getName();
  }

  return $options;
}
// phpcs:enable
