<?php

namespace Drupal\Tests\degov_devel\Kernel;

use Drupal\Core\Extension\Extension;
use Drupal\KernelTests\KernelTestBase;

/**
 * Class ModuleInstallationTest.
 */
class ModuleInstallationTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'degov_devel',
    'devel',
    'webprofiler'
  ];

  /**
   * Test module can be installed and uninstalled.
   */
  public function testModuleCanBeInstalledAndUninstalled(): void {
    /**
     * @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
     */
    $moduleHandler = $this->container->get('module_handler');
    $this->enableModules(['degov_devel']);
    self::assertTrue($moduleHandler->moduleExists('degov_devel'));
    self::assertTrue($moduleHandler->moduleExists('devel'));
    self::assertTrue($moduleHandler->moduleExists('webprofiler'));

    $this->container->get('module_installer')->uninstall([
      'degov_devel',
      'webprofiler',
      'devel',
    ]);
    self::assertInstanceOf(Extension::class, $moduleHandler->getModule('degov_devel'));
    self::assertInstanceOf(Extension::class, $moduleHandler->getModule('devel'));
    self::assertInstanceOf(Extension::class, $moduleHandler->getModule('webprofiler'));
  }

}
