<?php

/**
 * @file
 * Drupal hooks implementations for the degov_paragraph_map module.
 */

declare(strict_types=1);

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_theme().
 */
function degov_paragraph_map_theme($existing, $type, $theme, $path) {
  return [
    'paragraph__map' => [
      'base hook' => 'paragraph',
      'template' => 'paragraph--map',
    ],
    'paragraph__map__preview' => [
      'base hook' => 'paragraph',
      'template' => 'paragraph--map--preview',
    ],
  ];
}

/**
 * Implements hook_field_widget_form_alter().
 */
function degov_paragraph_map_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  /** @var \Drupal\Core\Field\WidgetBase $widget */
  $widget = $context['widget'];

  // Limit the view mode selection field to the selected view modes.
  if (!empty($element['subform']['field_map_address_view_mode']) && $widget->getPluginId() === 'paragraphs') {
    $allowed_view_modes = [
      'osm_map'     => t('OSM Map'),
      'default_map' => t('Google Map'),
    ];
    $element['subform']['field_map_address_view_mode']['widget']['#options'] = $allowed_view_modes;
  }
}
