<?php

declare(strict_types=1);

namespace Drupal\Tests\degov_theming\Unit;

use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\degov_theming\TwigExtension\IsEmptyValueExtension;
use Drupal\Tests\UnitTestCase;

/**
 * Class IsEmptyValueExtensionTest.
 */
class IsEmptyValueExtensionTest extends UnitTestCase {

  /**
   * Extension.
   *
   * @var \Drupal\degov_theming\TwigExtension\IsEmptyValueExtension
   */
  protected $extension;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = $this->createMock(RendererInterface::class);
    /** @var \Drupal\Core\Routing\UrlGeneratorInterface $url_generator */
    $url_generator = $this->createMock(UrlGeneratorInterface::class);
    /** @var \Drupal\Core\Theme\ThemeManagerInterface $theme_manager */
    $theme_manager = $this->createMock(ThemeManagerInterface::class);
    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = $this->createMock(DateFormatterInterface::class);
    /** @var \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory */
    $loggerFactory = $this->createMock(LoggerChannelFactory::class);
    $this->extension = new IsEmptyValueExtension($renderer, $url_generator, $theme_manager, $date_formatter, $loggerFactory);
  }

  /**
   * Is empty.
   *
   * @dataProvider providerTestIsEmpty
   */
  public function testIsEmpty($expected_result, $render_array, $stripTags = ''): void {
    $result = $this->extension->isEmpty($render_array, $stripTags);
    self::assertSame($expected_result, $result);
  }

  /**
   * Provider test is empty.
   */
  public function providerTestIsEmpty(): \Iterator {
    yield [FALSE, 'This is a test a simple test'];
    yield [FALSE, '   this is a test with text, padding

         and returns   ',
    ];
    yield [FALSE, '   this is a test with text, left padding

         and returns',
    ];
    yield [FALSE, "   this is a test with text, left padding

         and returns", "",
    ];
    yield [FALSE, '<p><h1>This is a test with tags</h1>this is a test</p>'];
    yield [FALSE, '<p>
                <h1>This is a test with tags</h1>
                this is a test
              </p>',
    ];
    yield [FALSE, '    <p><h1>This is a test with tags and padding</h1>this is a test</p>     '];
    yield [FALSE, '    <p><h1>This is a test with tags and left padding</h1>this is a test</p>'];
    yield [FALSE, '<img src="http://www.example.com/image.png" alt="This test only contains tags" />'];
    yield [
      TRUE,
      '<img src="http://www.example.com/image.png" alt="This test only contains tags">',
      '<blockquote>',
    ];
    yield [TRUE, ''];
    yield [TRUE, '\n\r       '];
    yield [TRUE, '     \n\r       ', ''];
    yield [TRUE, "\n\r       "];
    yield [TRUE, "     \n\r       ", ""];
    yield [FALSE, '\n\rthis is a test with characterMask and right padding       '];
    yield [FALSE, [
      '#prefix' => '<p class="wrapper">',
      '#suffix' => '</p>',
      '#markup' => 'This is a markup renderable array',
      '#printed' => TRUE,
    ],
    ];
    yield [FALSE, [
      '0' => [
        'target_id' => '54',
      ],
    ],
    ];
  }

}
