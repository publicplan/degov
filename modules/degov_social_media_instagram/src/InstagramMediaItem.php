<?php

declare(strict_types=1);

namespace Drupal\degov_social_media_instagram;

/**
 * WARNING: Do not change the case of properties with codingStandardsIgnoreLine.
 */

/**
 * Class InstagramMediaItem
 *
 * @package Drupal\degov_social_media_instagram
 */
class InstagramMediaItem {

  /**
   * @var string
   */
  private $caption;

  /**
   * @var string
   */
  // @codingStandardsIgnoreLine
  private $media_url;

  /**
   * @var string
   */
  private $username;

  /**
   * @var string
   */
  private $permalink;

  /**
   * @var string
   */
  // @codingStandardsIgnoreLine
  private $media_type;

  /**
   * @var int
   */
  private $timestamp;

  /**
   * InstagramMediaItem constructor.
   */
  public function __construct(array $values) {
    $this->caption = $values['caption'] ?? '';
    $this->media_url = $values['media_url'];
    $this->username = $values['username'];
    $this->permalink = $values['permalink'] ?? '';
    $this->media_type = $values['media_type'];
    if (\is_string($values['timestamp'])) {
      $this->setTimestamp($values['timestamp']);
    }
    else {
      $this->timestamp = $values['timestamp'];
    }
    if (!empty($values['thumbnail_url'])) {
      $this->media_url = $values['thumbnail_url'];
    }
  }

  public function getCaption(): string {
    return $this->caption;
  }

  public function getMediaUrl(): string {
    return $this->media_url;
  }

  public function getUserName(): string {
    return $this->username;
  }

  public function getPermalink(): string {
    return $this->permalink;
  }

  public function getMediaType(): string {
    return $this->media_type;
  }

  public function getTimestamp(): ?int {
    return $this->timestamp;
  }

  public function setTimestamp(string $timestamp): InstagramMediaItem {
    $timestamp_converted = \strtotime($timestamp);
    if ($timestamp_converted !== FALSE && $timestamp_converted > 0) {
      $this->timestamp = $timestamp_converted;
    }
    return $this;
  }

  public static function create(array $values): InstagramMediaItem {
    return new self($values);
  }

  public function toArray(): array {
    return \get_object_vars($this);
  }

}
