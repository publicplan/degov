<?php

namespace Drupal\Tests\media_file_links\Kernel;

use Drupal\Tests\degov_common\Kernel\ModuleInstallationTestAbstract;

/**
 * Class ModuleInstallationTest.
 *
 * @package Drupal\Tests\media_file_links\Kernel
 */
class ModuleInstallationTest extends ModuleInstallationTestAbstract {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'degov_media_usage',
    'media_file_links',
  ];

}
