<?php

declare(strict_types=1);

namespace Drupal\media_file_links;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Class MediaFileLinksServiceProvider
 *
 * @package Drupal\media_file_links
 */
class MediaFileLinksServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Remove the service until degov_media_usage is installed.
    if ($container->has('degov_media_usage.persistance') === FALSE) {
      $container->removeDefinition('media_file_links.media_tracker');
    }
  }

}
