<?php

declare(strict_types=1);

namespace Drupal\Tests\degov_rich_text_format_settings\Kernel;

use Drupal\Core\Extension\Extension;
use Drupal\KernelTests\KernelTestBase;

/**
 * Class InstallationTest.
 */
class ModuleInstallationTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'editor',
    'entity_embed',
    'spamspan',
    'ckeditor',
    'linkit',
    'node',
    'degov_rich_text_format_settings'
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['degov_rich_text_format_settings']);
  }

  /**
   * Tests that the module can be installed and is available.
   */
  public function testSetup(): void {
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler */
    $moduleHandler = $this->container->get('module_handler');
    self::assertInstanceOf(Extension::class, $moduleHandler->getModule('degov_rich_text_format_settings'));
  }

  public function testTextFormatWeight(): void {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $moduleHandler */
    $moduleHandler = $this->container->get('module_installer');
    $moduleHandler->install(['user', 'config_replace']);
    $moduleHandler->install(['lightning_media']);
    // The Filter module provides plain_text.
    // The lightning_media module provides rich_text which is overridden via degov_rich_text_format_settings.
    $this->installConfig(['filter', 'lightning_media']);
    $this->container->get('config_replace.config_replacer')->rewriteModuleConfig('degov_rich_text_format_settings');

    self::assertFalse($this->config('filter.format.rich_text')->isNew(), 'rich_text text format is installed');
    self::assertSame(-30, $this->config('filter.format.rich_text')->get('weight'));

    self::assertFalse($this->config('filter.format.rich_html')->isNew(), 'rich_html text format is installed');
    self::assertSame(-20, $this->config('filter.format.rich_html')->get('weight'));

    self::assertFalse($this->config('filter.format.minimal_html')->isNew(), 'minimal_html text format is installed');
    self::assertSame(-10, $this->config('filter.format.minimal_html')->get('weight'));

    self::assertFalse($this->config('filter.format.plain_text')->isNew(), 'plain_text text format is installed');
    self::assertSame(10, $this->config('filter.format.plain_text')->get('weight'));
  }

}
