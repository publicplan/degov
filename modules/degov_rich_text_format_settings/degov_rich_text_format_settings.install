<?php

/**
 * @file
 * Install, update and uninstall hooks for the degov_rich_text_format_settings module.
 */

declare(strict_types=1);

use Symfony\Component\Yaml\Yaml;

/**
 * Implements hook_update_last_removed().
 */
function degov_rich_text_format_settings_update_last_removed(): int {
  return 870100;
}

/**
 * deGov 8.4.5 - Adds a minimal HTML input filter.
 */
function degov_rich_text_format_settings_update_880405() {
  $configsToInstall = [
    'editor.editor.minimal_html',
    'filter.format.minimal_html',
  ];

  foreach ($configsToInstall as $configName) {
    $config = \Drupal::configFactory()->getEditable($configName);
    $config->setData(Yaml::parseFile(\Drupal::moduleHandler()
      ->getModule('degov_rich_text_format_settings')
      ->getPath() . '/config/install/' . $configName . '.yml'))->save();
  }

  $configsToLocalize = [
    'de' => [
      'editor.editor.minimal_html',
      'filter.format.minimal_html',
    ]
  ];

  foreach ($configsToLocalize as $langcode => $configs) {
    foreach ($configs as $configName) {
      $configOverride = \Drupal::service('language.config_factory_override')
        ->getOverride($langcode, $configName);
      $pathToConfigFile = \Drupal::moduleHandler()
        ->getModule('degov_rich_text_format_settings')
        ->getPath() . '/config/install/language/' . $langcode . '/' . $configName . '.yml';
      $configOverride->setData(Yaml::parseFile($pathToConfigFile));
      $configOverride->save();
    }
  }
}

/**
 * deGov 8.5.0
 *  - Install editor_advanced_link.
 */
function degov_rich_text_format_settings_update_880500(): void {
  // The functionality of editor_advanced_link was part of linkit 4.x.
  // In version 5.x this now part of editor_advanced_link.
  /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
  $module_installer = \Drupal::service('module_installer');
  $module_installer->install(['editor_advanced_link']);
}

/**
 * Update text formats weight.
 */
function degov_rich_text_format_settings_update_900001() {
  $config_factory = \Drupal::configFactory();
  $formats = $config_factory->listAll('filter.format.');
  foreach ($formats as $config_name) {
    $format = $config_factory->getEditable($config_name);
    // 0 is the default weight.
    $weight = 0;
    switch ($config_name) {
      case 'filter.format.rich_text':
        $format->set('weight', -30);
        break;

      case 'filter.format.rich_html':
        $format->set('weight', -20);
        break;

      case 'filter.format.minimal_html':
        $format->set('weight', -10);
        break;

      case 'filter.format.plain_text':
        // See core/modules/filter/config/install/filter.format.plain_text.yml:10.
        $format->set('weight', 10);
        break;

      default:
        $weight++;
        $format->set('weight', $weight);
    }
    $format->save();
  }
}

/**
 * Remove filter_autop from Text format minimal_html.
 */
function degov_rich_text_format_settings_update_900002() {
  $config_factory = \Drupal::configFactory();
  $format = $config_factory->getEditable('filter.format.minimal_html');
  $data = $format->getRawData();
  $isChanged = FALSE;
  if (isset($data['filters']['filter_autop'])) {
    unset($data['filters']['filter_autop']);
    $isChanged = TRUE;
  }
  if (isset($data['filters']['filter_html'])) {
    if (strpos($data['filters']['filter_html']['settings']['allowed_html'], '<br>') === FALSE) {
      $allowedTags = $data['filters']['filter_html']['settings']['allowed_html'] . ' <br>';
      $data['filters']['filter_html']['settings']['allowed_html'] = $allowedTags;
      $isChanged = TRUE;
    }
  }
  if ($isChanged) {
    $format->setData($data);
    $format->save();
  }
}

/**
 * Update rich text filter sequence and add missing allowed (alt title) html attributes.
 */
function degov_rich_text_format_settings_update_900003() {
  $configFactory = \Drupal::configFactory();
  $editableConfig = $configFactory->getEditable('filter.format.rich_text');

  if (!$editableConfig->isNew()) {
    // Set missing drupal-entity attributes.
    $newValue = "<a href hreflang target data-entity-type data-entity-uuid data-entity-substitution> <em> <strong> <cite> <blockquote cite> <code> <ul type> <ol start type> <li> <dl> <dt> <dd> <h2 id> <h3 id> <h4 id> <h5 id> <h6 id> <s> <sup> <sub> <img src alt data-entity-type data-entity-uuid data-align data-caption> <caption> <hr> <p> <br> <pre> <drupal-entity data-* alt title> <table border cellspacing cellpadding width frame align style ><tr rowspan width height align valign bgcolor background bordercolor > <tbody> <thead> <tfoot> <td colspan rowspan width height align valign bgcolor background bordercolor scope> <th colspan rowspan width height align valign scope >";
    $editableConfig->set('filters.filter_html.settings.allowed_html', $newValue);

    // Move filter sequences in the correct order.
    $editableConfig->set('filters.filter_htmlcorrector.weight', -45);
    $editableConfig->set('filters.editor_file_reference.weight', -44);
    $editableConfig->set('filters.entity_embed.weight', -46);
    $editableConfig->save();
  }
}
