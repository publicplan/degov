<?php

declare(strict_types=1);

namespace Drupal\degov_rich_text_format_settings;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\User;

/**
 * Defines a service for Text Editor's render elements.
 */
class Element implements TrustedCallbackInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['preRenderTextFormat'];
  }

  /**
   * Additional #pre_render callback for 'text_format' elements.
   */
  public function preRenderTextFormat(array $element): array {
    if (!isset($element['format'])) {
      return $element;
    }
    $current_user = \Drupal::currentUser();
    if ($current_user->isAnonymous()) {
      return $element;
    }
    /** @var \Drupal\user\UserInterface $user */
    $user = User::load($current_user->id());
    $has_roles = $user->hasRole('administrator') || $user->id() == 1 || $user->hasRole('manager');
    if (\array_key_exists('rich_text', $element['format']['format']['#options']) && $has_roles) {
      if (\array_key_exists('#description', $element['format']['format']) === FALSE) {
        $element['format']['format']['#description'] = '';
      }
      else {
        $element['format']['format']['#description'] .= '<br>';
      }
      $label = $element['format']['format']['#options']['rich_text'];
      $element['format']['format']['#description'] .= '<p>' . $this->t('When creating content, please make sure to use a text format that can be used by as many roles as possible. The %text_format format can be used by most roles.', ['%text_format' => $label]) . '</p>';
    }
    return $element;
  }

}
