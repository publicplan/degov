<?php

/**
 * @file
 * Media Audio.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_theme().
 */
function degov_media_audio_theme($existing, $type, $theme, $path) {
  return [
    'media__audio' => [
      'base hook' => 'media',
      'template' => 'media--audio',
    ],
    'media__audio__embedded' => [
      'base hook' => 'media',
      'template' => 'media--audio--embedded',
    ],
    'media__audio__full' => [
      'base hook' => 'media',
      'template' => 'media--audio--full',
    ],
    'media__audio__preview' => [
      'base hook' => 'media',
      'template' => 'media--audio--preview',
    ],
    'media__audio__preview_wide' => [
      'base hook' => 'media',
      'template' => 'media--audio--preview-wide',
    ],
    'media__audio__reference' => [
      'base hook' => 'media',
      'template' => 'media--audio--reference',
    ],
    'media__audio__search' => [
      'base hook' => 'media',
      'template' => 'media--audio--search',
    ],
    'media__audio__usage' => [
      'base hook' => 'media',
      'template' => 'media--audio--usage',
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function degov_media_audio_preprocess_media__audio(&$variables) {
  /* @var \Drupal\media\Entity\Media $media */
  $media = $variables['elements']['#media'];
  $variables['#attached']['library'][] = 'degov_media_audio/audio';
  $variables['audio_url'] = $media->toUrl();
  $title = $media->get('field_title')->first()->value;
  // Todo Subtitle is optional but the only visible Element in template.
  $subtitle = $media->get('field_subtitle')->first()->value;
  $variables['audio_attributes'] = new Attribute([
    'id' => 'audio-' . $media->id(),
    'controls' => 'controls',
    'title' => empty($subtitle) ? $title : ($title . '. ' . $subtitle),
  ]);

  $variables['content']['audio_date'] = \Drupal::service('date.formatter')->format($variables['elements']['#media']->created->value, 'date_medium');
  // Check if the download of the video is allowed by media settings.
  $download_is_allowed = FALSE;
  if ($media->hasField('field_allow_download') && !$media->get('field_allow_download')->isEmpty()) {
    $download_is_allowed = $media->get('field_allow_download')->value ? TRUE : FALSE;
  }

  if (!$media->get('field_audio_mp3')->isEmpty()) {
    /* @var \Drupal\file\Entity\File $file */
    $file = $media->field_audio_mp3->entity;
    if (!empty($file)) {
      $variables['audio_mp3'] = file_create_url($file->getFileUri());
      if (in_array($variables['elements']['#view_mode'], ['full', 'default'])) {
        // If allowed let's prepare the links.
        if ($download_is_allowed) {
          $variables['content']['download_mp3'] = degov_common_file_download_render_array($file);
        }
      }
    }
  }

  if (!$media->get('field_audio_ogg')->isEmpty()) {
    /* @var \Drupal\file\Entity\File $file */
    $file = $media->field_audio_ogg->entity;
    if (!empty($file)) {
      $variables['audio_ogg'] = file_create_url($file->getFileUri());
      if (in_array($variables['elements']['#view_mode'], ['full', 'default'])) {
        // If allowed let's prepare the links.
        if ($download_is_allowed) {
          $variables['content']['download_ogg'] = degov_common_file_download_render_array($file);
        }
      }
    }
  }

  degov_media_plyr_attach_to_render_array($variables);
  $variables['created'] = $variables['elements']['#media']->created->value;
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function degov_media_audio_media_presave(EntityInterface $entity) {
  $bundle = $entity->bundle();
  if ($bundle === 'audio') {
    // Check if the field for duration is enabled for the media bundle and
    // that it is empty. There is no need to check the duration again and
    // again if it is already set.
    if ($entity->hasField('field_media_duration') && $entity->get('field_media_duration')->isEmpty()) {
      /** @var \Drupal\degov_common\VideoUtils $videoUtils */
      $videoUtils = \Drupal::service('degov_common.video_utils');
      $duration = $videoUtils->getVideoDuration($entity);
      $entity->set('field_media_duration', [0 => ['value' => $duration]]);
    }
  }
}
