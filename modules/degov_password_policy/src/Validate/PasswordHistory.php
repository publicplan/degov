<?php

declare(strict_types = 1);

namespace Drupal\degov_password_policy\Validate;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class PasswordHistory.
 *
 * @package Drupal\degov_password_policy\Validate
 */
class PasswordHistory implements ValidateInterface {

  /**
   * @inheritdoc
   */
  public static function validate(array $form, FormStateInterface $formState): void {
    $degov_password_policy_settings = Settings::get('degov_password_policy');
    if ($degov_password_policy_settings && !empty($degov_password_policy_settings['skip_checks'])) {
      return;
    }

    $uid = (int) $formState->getFormObject()->getEntity()->id();
    /** @var string|NULL $newPass */
    $newPass = $formState->getValue('pass');

    // Skip empty field.
    if (empty($newPass)) {
      return;
    }

    /** @var \Drupal\degov_password_policy\Service\PasswordHistoryService $historyService */
    $historyService = \Drupal::service('degov_password_policy.service.password_history');
    $isPasswordInHistory = $historyService->isPasswordInHistory($uid, $newPass);

    if ($isPasswordInHistory) {
      $formState->setError(
        $form['account']['pass'],
        (string) new TranslatableMarkup('Password has been used already. Choose a different password.')
      );
    }
  }

}
