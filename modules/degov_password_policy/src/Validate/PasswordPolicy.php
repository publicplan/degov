<?php

declare(strict_types = 1);

namespace Drupal\degov_password_policy\Validate;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use function count;
use function preg_match;
use function strlen;

/**
 * Class PasswordPolicy.
 *
 * @package Drupal\degov_password_policy\Validate
 */
class PasswordPolicy implements ValidateInterface {

  /**
   * @inheritdoc
   */
  public static function validate(array $form, FormStateInterface $formState): void {
    /** @var string|NULL $value */
    $value = $formState->getValue('pass');

    // Skip empty field.
    if (empty($value)) {
      return;
    }
    $degov_password_policy_settings = Settings::get('degov_password_policy');
    if ($degov_password_policy_settings && !empty($degov_password_policy_settings['skip_checks'])) {
      return;
    }

    $errors = [];
    if (strlen($value) < 12) {
      $errors['password_length'] = new TranslatableMarkup('Password should be at least 12 characters long');
    }
    if (!preg_match('/\d/', $value)) {
      $errors['password_contains_digit'] = new TranslatableMarkup('Password should contain at least one digit');
    }
    if (!preg_match('/[A-Z]/', $value)) {
      $errors['password_contains_uc_letter'] = new TranslatableMarkup('Password should contain at least one upper-case letter');
    }
    if (!preg_match('/[a-z]/', $value)) {
      $errors['password_contains_lc_letter'] = new TranslatableMarkup('Password should contain at least one lower-case letter');
    }
    if (!preg_match('/[^a-zA-Z0-9]/', $value)) {
      $errors['password_contains_special_character'] = new TranslatableMarkup('Password should contain at least one special character');
    }

    if ((\is_countable($errors) ? count($errors) : 0) > 0) {
      $errorString = '<ul>';
      foreach ($errors as $error) {
        $errorString .= "<li>$error</li>";
      }
      $errorString .= '</ul>';
      $formState->setError($form['account']['pass'], (string) new FormattableMarkup($errorString, $errors));
    }
  }

}
