<?php

declare(strict_types=1);

namespace Drupal\degov_media_usage_entity_embed;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\degov_media_usage\Service\MediaUsagePersistance;
use Drupal\entity_embed\Exception\EntityNotFoundException;
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class MediaEntityEmbedUsageTracker
 *
 * @package Drupal\degov_media_usage_entity_embed
 */
class MediaEntityEmbedUsageTracker extends MediaUsagePersistance {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var int[]
   */
  private $mediaIds = [];

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function setEntityManager(EntityTypeManagerInterface $entityTypeManager): void {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @inheritDoc
   */
  public function canHandle(EntityInterface $entity): bool {
    return $entity instanceof NodeInterface || $entity instanceof ParagraphInterface;
  }

  /**
   * @inheritDoc
   */
  public function getMedia(EntityInterface $entity): array {
    $this->mediaIds = [];
    $fields = $entity->getFields(FALSE);
    foreach ($fields as $field) {
      $fd = $field->getFieldDefinition();
      $type = $fd->getType();
      if ($type !== 'text_long') {
        continue;
      }
      $text = $field->getString();
      if ((\strpos($text, 'data-entity-type') !== FALSE && (\strpos($text,
            'data-entity-embed-display') !== FALSE || \strpos($text,
            'data-view-mode') !== FALSE))) {
        $dom = Html::load($text);
        $xpath = new \DOMXPath($dom);

        foreach ($xpath->query('//drupal-entity[@data-entity-type and (@data-entity-uuid or @data-entity-id) and (@data-entity-embed-display or @data-view-mode)]') as $node) {
          /** @var \DOMElement $node */
          $entity_type = $node->getAttribute('data-entity-type');
          if ($entity_type !== 'media') {
            continue;
          }
          try {
            // Load the entity either by UUID (preferred) or ID.
            $id = NULL;
            if ($id = $node->getAttribute('data-entity-uuid')) {
              $entities = $this->entityTypeManager->getStorage($entity_type)
                ->loadByProperties(['uuid' => $id]);
              $entity = \current($entities);
            }
            else {
              $id = $node->getAttribute('data-entity-id');
              $entity = $this->entityTypeManager->getStorage($entity_type)
                ->load($id);
            }
            if (!$entity instanceof MediaInterface) {
              continue;
            }
          }
          catch (EntityNotFoundException $e) {
            continue;
          }

          if ($entity instanceof MediaInterface) {
            $this->mediaIds[] = $entity->id();
          }
        }
      }
    }
    return $this->mediaIds;
  }

}
