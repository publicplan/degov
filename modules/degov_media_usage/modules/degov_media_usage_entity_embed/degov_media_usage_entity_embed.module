<?php

/**
 * @file
 * Drupal hooks implementations.
 */

declare(strict_types=1);


use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_entity_insert().
 */
function degov_media_usage_entity_embed_entity_insert(EntityInterface $entity) {
  _degov_media_usage_entity_embed_track($entity);
}

/**
 * Implements hook_entity_update().
 */
function degov_media_usage_entity_embed_entity_update(EntityInterface $entity) {
  _degov_media_usage_entity_embed_track($entity);
}

function _degov_media_usage_entity_embed_track(EntityInterface $entity) {
  /** @var \Drupal\degov_media_usage_entity_embed\MediaEntityEmbedUsageTracker $tracker */
  $tracker = \Drupal::service('degov_media_usage_entity_embed.tracker');
  if ($tracker->canHandle($entity)) {
    $tracker->purge($entity);
    $tracker->store($entity, $tracker->getMedia($entity));
  }
}

/**
 * Implements hook_entity_delete().
 */
function degov_media_usage_entity_embed_entity_delete(EntityInterface $entity): void {
  /** @var \Drupal\degov_media_usage_entity_embed\MediaEntityEmbedUsageTracker $tracker */
  $tracker = \Drupal::service('degov_media_usage_entity_embed.tracker');
  if ($tracker->canHandle($entity)) {
    $tracker->purge($entity);
  }
}
