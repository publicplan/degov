<?php

namespace Drupal\Tests\filter_disallow\Kernel;

use Drupal\Tests\degov_common\Kernel\ModuleInstallationTestAbstract;

/**
 * Class ModuleInstallationTest.
 */
class ModuleInstallationTest extends ModuleInstallationTestAbstract {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter_disallow',
  ];

}
