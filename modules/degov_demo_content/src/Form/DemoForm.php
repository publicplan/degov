<?php

declare(strict_types=1);

namespace Drupal\degov_demo_content\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DemoForm
 *
 * @package Drupal\degov_demo_content\Form
 */
class DemoForm extends FormBase {

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'demoform';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['checkbox'] = [
      '#type' => 'checkbox',
      '#title' => 'Checkbox without description'
    ];
    $form['checkbox_description'] = [
      '#type' => 'checkbox',
      '#title' => 'Checkbox with description',
      '#description' => 'Check this for a better world',
    ];

    $form['checkboxes'] = [
      '#type' => 'checkboxes',
      '#title' => 'Checkboxes without description',
      '#options' => [0 => 'Option 0', 1 => 'Option 1'],
    ];
    $form['checkboxes_description'] = [
      '#type' => 'checkboxes',
      '#title' => 'Checkboxes with description',
      '#options' => [0 => 'Option 0', 1 => 'Option 1'],
      '#description' => 'Check this for a better world',
    ];
    $form['checkboxes_description_per_checkbox'] = [
      '#type' => 'checkboxes',
      '#title' => 'Checkboxes with description and description for option 1',
      '#options' => [0 => 'Option 0', 1 => 'Option 1'],
      '#description' => 'Check this for a better world',
    ];
    $form['checkboxes_description_per_checkbox'][1]['#description'] = 'Description option 1';
    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
