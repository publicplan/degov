<?php

declare(strict_types=1);

namespace Drupal\degov_demo_content\SocialMedia;

use Drupal\degov_social_media_instagram\Instagram as OriginInstagram;

/**
 * Class Instagram
 *
 * @package Drupal\degov_demo_content\SocialMedia
 */
class Instagram extends OriginInstagram {

  use SocialMediaAssetsTrait;

  /**
   * {@inheritDoc}
   */
  public function getMedias(int $count = 20): array {
    return self::deserialize('instagram', 'medias', 'Drupal\degov_social_media_instagram\InstagramMediaItem') ?? [];
  }

  public static function updateMedias() {
    $instance = OriginInstagram::create(\Drupal::getContainer());
    $medias = $instance->getMedias(\Drupal::configFactory()->get('degov_social_media_instagram.settings')->get('number_of_posts'));
    try {
      if (self::serialize('instagram', 'medias', $medias)) {
        \Drupal::messenger()->addStatus('Updated demo data for instagram');
      }
    }
    catch (\Exception $exception) {
      \Drupal::messenger()->addError($exception->getMessage());
      \watchdog_exception('degov_demo_content', $exception);
    }

  }

}
