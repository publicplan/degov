<?php

namespace Drupal\degov_demo_content\SocialMedia;

/**
 * Trait SocialMediaAssetsTrait.
 *
 * @package Drupal\degov_demo_content
 * @internal Used for demo data.
 */
trait SocialMediaAssetsTrait {

  /**
   * Returns the content from storage file.
   *
   * @param string $provider
   *   The name of the social media provider.
   * @param string $fileName
   *   The name of file.
   *
   * @return mixed
   *   Unserialized data.
   */
  private static function getDataFromFile(string $provider, string $fileName) {
    $path = [
      \drupal_get_path('module', 'degov_demo_content'),
      'fixtures',
      'social_media',
      $provider,
      $fileName,
    ];
    // TODO refactor this.
    $filePath = \implode('/', $path);
    $content = \file_get_contents($filePath);
    $options = ['allowed_classes' => TRUE];
    return \unserialize($content, $options);
  }

  private static function serialize(string $provider, string $fileName, $data) {
    /** @var \Symfony\Component\Serializer\Serializer $serializer */
    $serializer = \Drupal::service('serializer');
    $serializer_data = $serializer->serialize($data, 'json');
    $path = [
      \drupal_get_path('module', 'degov_demo_content'),
      'fixtures',
      'social_media',
      $provider,
      $fileName,
    ];
    $filePath = \implode('/', $path);
    return \file_put_contents($filePath . '.json', $serializer_data);
  }

  private static function deserialize(string $provider, string $fileName, string $type) {
    if (!\Drupal::hasService('serializer')) {
      return NULL;
    }

    $path = [
      \drupal_get_path('module', 'degov_demo_content'),
      'fixtures',
      'social_media',
      $provider,
      $fileName,
    ];
    $filePath = \implode('/', $path);
    $content = \file_get_contents($filePath . '.json');

    /** @var \Symfony\Component\Serializer\Serializer $serializer */
    $serializer = \Drupal::service('serializer');
    return $serializer->deserialize($content, $type, 'json');
  }

}
