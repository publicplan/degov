<?php

namespace Drupal\Tests\degov_date_formats\Kernel;

use Drupal\Tests\degov_common\Kernel\ModuleInstallationTestAbstract;

/**
 * Class ModuleInstallationTest.
 */
class ModuleInstallationTest extends ModuleInstallationTestAbstract {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'degov_date_formats',
  ];

}
