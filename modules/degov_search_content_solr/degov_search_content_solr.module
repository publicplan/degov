<?php

/**
 * @file
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_facets_search_api_query_type_mapping_alter().
 */
function degov_search_content_solr_facets_search_api_query_type_mapping_alter($backend_plugin_id, array &$query_types) {
  $query_types['date'] = 'search_api_degov_date_range';
}

/**
 * Implements hook_form_views_exposed_form_alter().
 */
function degov_search_content_solr_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Helper to add theme hook suggestions to exposed form fields.
  if ($form_id === 'views_exposed_form') {
    $view_id = $form_state->getStorage()['view']->id();
    _degov_search_content_solr_add_view_id_to_input_elements($form, $view_id);
  }
}

/**
 * Helper to add view Id to input Elements.
 */
function _degov_search_content_solr_add_view_id_to_input_elements(array &$root_element, $view_id) {
  $input_types = ['textfield', 'submit'];
  if (in_array($root_element['#type'], $input_types)) {
    $root_element['#degov_search_content_solr_view_id'] = $view_id;
  }
  foreach (Element::children($root_element) as $key) {
    _degov_search_content_solr_add_view_id_to_input_elements($root_element[$key], $view_id);
  }
}

/**
 * Implements hook_theme_suggestions_input_alter().
 */
function degov_search_content_solr_theme_suggestions_input_alter(&$suggestions, array $variables) {
  // Suggest view id based input template.
  if (isset($variables['element']['#degov_search_content_solr_view_id'])) {
    $elm = $variables['element'];
    $suggestions[] = 'input_' . $elm['#type'] . '__' . $elm['#degov_search_content_solr_view_id'] . '__' . $elm['#name'];
  }
}

/**
 * Implements hook_preprocess().
 */
function degov_search_content_solr_preprocess(&$variables, $hook) {
  if (isset($variables['element']['#degov_search_content_solr_view_id'])) {

    // Improve frontend-validation and accessibility.
    if ($variables['element']['#type'] === 'textfield') {
      $variables['element']['#attributes']['minlength'] = 3;
      $variables['element']['#attributes']['pattern'] = '\s*(\S\s*){3,}';
      $variables['element']['#attributes']['title'] = t('Please enter a search term with at least 3 letters');
      $variables['element']['#attributes']['autocomplete'] = 'off';
    }
  }
}
