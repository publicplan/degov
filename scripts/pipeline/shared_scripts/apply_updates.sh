#!/usr/bin/env bash

set -o nounset
set -o pipefail
set -o errexit

if [[ -n "${DEBUG:-}" ]];then
  set -o xtrace
fi

# shellcheck disable=SC2164
__DIR__="$(cd "$(dirname "${0}")"; pwd)"

# shellcheck source=.
source "$__DIR__/../.env"

# shellcheck source=.
source "$__DIR__/common_functions.sh"

bash "$__DIR__/start_services.sh"

cd project

_setup_settings_php
_setup_file_system
_setup_local_settings_php
_info "### Drop any existing db"
_drush sql:drop
_info "### Importing db dump"
cp $TEST_DIR/lfs_data/$DB_DUMP.sql.gz /tmp/db.sql.gz
_drush sql-query --file="/tmp/db.sql.gz"
_copy_assets
_info "### Run database updates"
_drush updb
_info "### Delete old watchdog entries from db dump"
_drush watchdog:delete all
_info "### Clear cache"
_drush cr
_disable_geocoder_presave
_info "### Install the degov_demo_content"
_drush en degov_demo_content
_update_translations
_drush_watchdog
_create_db_dump
# We restore the profile in default_setup_ci.sh.
_info "Purge $PROFILE_DIR"
rm -rf "$PROFILE_DIR"
find . -type d -name '.git' -print0 -exec rm -rf {} + >/dev/null
