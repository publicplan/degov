#!/usr/bin/env bash

set -o nounset
set -o pipefail
set -o errexit

if [[ -n "${DEBUG:-}" ]];then
  set -o xtrace
fi

# shellcheck disable=SC2164
__DIR__="$(cd "$(dirname "${0}")"; pwd)"

# shellcheck source=.
source "$__DIR__/../.env"

# shellcheck source=.
source "$__DIR__/common_functions.sh"

bash "$__DIR__/start_services.sh"

cd project

_setup_file_system
_setup_settings_php
_info "### Installing a new"
_behat -c behat-no-drupal.dist.yml
_disable_geocoder_presave
_info "### Install the degov_demo_content"
_drush en degov_demo_content
_update_translations
_drush_watchdog
_create_db_dump
# We restore the profile in default_setup_ci.sh.
_info "Purge $PROFILE_DIR"
rm -rf "$PROFILE_DIR"
find . -type d -name '.git' -print0 -exec rm -rf {} + >/dev/null

