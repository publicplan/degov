# $Id$
#
# German translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  degov_media_citation.info.yml: n/a
#  templates/media--citation--usage.html.twig: n/a
#  config/install/core.entity_view_display.media.citation.default.yml: n/a
#  config/install/core.entity_view_display.media.citation.preview.yml: n/a
#  config/install/core.entity_view_display.media.citation.usage.yml: n/a
#  config/install/field.field.media.citation.field_citation_date.yml: n/a
#  config/install/field.field.media.citation.field_citation_image.yml: n/a
#  config/install/field.field.media.citation.field_citation_text.yml: n/a
#  config/install/field.field.media.citation.field_citation_title.yml: n/a
#  config/install/field.field.media.citation.field_include_search.yml: n/a
#  config/install/field.field.media.citation.field_media_generic_2.yml: n/a
#  config/install/field.field.media.citation.field_media_in_library.yml: n/a
#  config/install/field.field.media.citation.field_tags.yml: n/a
#  config/install/field.field.media.citation.field_title.yml: n/a
#  config/install/media.type.citation.yml: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2019-11-19 12:57+0100\n"
"PO-Revision-Date: 2019-12-10 12:57+0100\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: German <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: degov_media_citation.info.yml:0
msgid "deGov - Media citation"
msgstr "deGov - Medium Zitat"

#: degov_media_citation.info.yml:0
msgid "Contains the citation media type with optional image."
msgstr "Enthält den Medientyp Zitat mit optionalem Bild."

#: degov_media_citation.info.yml:0
msgid "deGov"
msgstr "deGov"

#: templates/media--citation--usage.html.twig:44
msgid "Usage"
msgstr "Verwendung"

#: config/install/core.entity_view_display.media.citation.default.yml:0 config/install/core.entity_view_display.media.citation.preview.yml:0 config/install/core.entity_view_display.media.citation.usage.yml:0
msgctxt "PHP date format"
msgid "d.m.Y"
msgstr "t.m.J"

#: config/install/field.field.media.citation.field_citation_date.yml:0
msgid "Datum"
msgstr "Datum"

#: config/install/field.field.media.citation.field_citation_image.yml:0
msgid "Media"
msgstr "Medien"

#: config/install/field.field.media.citation.field_citation_text.yml:0
msgid "Text"
msgstr "Text"

#: config/install/field.field.media.citation.field_citation_title.yml:0
msgid "Titel"
msgstr "Titel"

#: config/install/field.field.media.citation.field_include_search.yml:0
msgid "Mediathek"
msgstr "Mediathek"

#: config/install/field.field.media.citation.field_include_search.yml:0
msgid "An"
msgstr "An"

#: config/install/field.field.media.citation.field_include_search.yml:0
msgid "Aus"
msgstr "Aus"

#: config/install/field.field.media.citation.field_media_generic_2.yml:0
msgid "Generic media"
msgstr "Allgemeines Medium"

#: config/install/field.field.media.citation.field_media_in_library.yml:0
msgid "Save to my media library"
msgstr "In meiner Mediathek speichern"

#: config/install/field.field.media.citation.field_media_in_library.yml:0
msgid "Saved to my media library"
msgstr "In meiner Mediathek gespeichert"

#: config/install/field.field.media.citation.field_media_in_library.yml:0
msgid "Not in my media library"
msgstr "Nicht in meiner Mediathek"

#: config/install/field.field.media.citation.field_tags.yml:0
msgid "Schlagwörter"
msgstr "Schlagwörter"

#: config/install/field.field.media.citation.field_title.yml:0
msgid "Öffentlicher Titel"
msgstr "Öffentlicher Titel"

#: config/install/media.type.citation.yml:0
msgid "Zitat"
msgstr "Zitat"

#: config/install/media.type.citation.yml:0
msgid "Ein Media-Bundle mit einem Zitat sowie einem optionalen Bild."
msgstr "Ein Media-Bundle mit einem Zitat sowie einem optionalen Bild."

