# $Id$
#
# German translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  degov_simplenews_references.info.yml: n/a
#  config/optional/core.entity_view_mode.node.email_plainalt.yml: n/a
#  config/optional/field.field.node.simplenews_issue.field_section.yml: n/a
#  config/optional/field.field.node.simplenews_issue.field_simplenews_issue_more.yml: n/a
#  config/optional/field.field.node.simplenews_issue.field_simplenews_issue_week.yml: n/a
#  config/optional/field.field.node.simplenews_issue.field_tags.yml: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2019-11-19 13:03+0100\n"
"PO-Revision-Date: 2019-12-10 12:57+0100\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: German <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: degov_simplenews_references.info.yml:0
msgid "deGov - Simplenews References"
msgstr "deGov - Simplenews Referenzen"

#: degov_simplenews_references.info.yml:0
msgid "Contains fields to link Simplenews issues to other content"
msgstr "Enthält Felder zum verlinken von Simplenews-Ausgaben mit anderen Inhalten"

#: degov_simplenews_references.info.yml:0
msgid "deGov"
msgstr "deGov"

#: config/optional/core.entity_view_mode.node.email_plainalt.yml:0
msgid "Email: Plain text alternative"
msgstr "E-Mail: Klartext-Alternative"

#: config/optional/field.field.node.simplenews_issue.field_section.yml:0
msgid "Bereich"
msgstr "Bereich"

#: config/optional/field.field.node.simplenews_issue.field_simplenews_issue_more.yml:0
msgid "Weitere Meldungen"
msgstr "Weitere Meldungen"

#: config/optional/field.field.node.simplenews_issue.field_simplenews_issue_week.yml:0
msgid "Meldungen der Woche"
msgstr "Meldungen der Woche"

#: config/optional/field.field.node.simplenews_issue.field_tags.yml:0
msgid "Schlagworte"
msgstr "Schlagworte"

